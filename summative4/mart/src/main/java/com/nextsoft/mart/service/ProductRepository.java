package com.nextsoft.mart.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.nextsoft.mart.entity.Products;

public interface ProductRepository extends JpaRepository<Products, Integer>{
	@Query (value="SELECT * FROM products WHERE product_name=?1", nativeQuery = true)
	List<Products> findProductName(String productName);
	
	@Query (value="SELECT * FROM products WHERE id=?1", nativeQuery = true)
	List<Products> findProductId(int id);
	
	@Query (value="SELECT * FROM products WHERE product_name=?1 OR category=?2 OR stock=?3", nativeQuery = true)
	List<Products> findProduct(String productName, String category, int stock);

	@Transactional
	@Modifying
	@Query (value="INSERT INTO products (product_name,category,stock,price,created_at) VALUES (?1,?2,?3,?4,?5)", nativeQuery = true)
	int add(String productName, String category, int stock, int price, String currentTime);
	
	@Transactional
	@Modifying
	@Query (value="UPDATE products SET product_name=?2,category=?3,stock=?4,price=?5 WHERE id=?1", nativeQuery = true)
	int update(int id ,String productName, String category, int stock, int price);
	
	@Transactional
	@Modifying
	@Query (value="UPDATE products SET stock=?2 WHERE id=?1", nativeQuery = true)
	int transaction(int id ,int stock);
	
	@Transactional
	@Modifying
	@Query (value="DELETE FROM products WHERE id=?1", nativeQuery = true)
	int delete(int id);

}