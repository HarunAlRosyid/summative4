package com.nextsoft.mart.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nextsoft.mart.entity.Products;
import com.nextsoft.mart.handler.ProductResponses;
import com.nextsoft.mart.handler.Responses;
import com.nextsoft.mart.service.ProductRepository;

@RestController
@RequestMapping("/api")
public class ProductController {
	List<Products> dataProduct = new ArrayList<Products>();
	List<Products> dataEmpty = new ArrayList<Products>();
	String currentTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	@Autowired
	ProductRepository repo;
	
	@GetMapping(value="/products", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Responses> getAllProduts() {
		dataProduct = repo.findAll();
		
		if(dataProduct.size()==0) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
				body(new ProductResponses(HttpStatus.INTERNAL_SERVER_ERROR.value(),"List of Produts is empty",dataEmpty));
		} else {
			return ResponseEntity.status(HttpStatus.OK).
				body(new ProductResponses(HttpStatus.OK.value(),"List of Produts",dataProduct));
		}	
	}

	
	@PostMapping(value="/product", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Responses>  addProduct(@RequestBody List<Products> payload) {
		if(payload.size()>0) {
			payload.forEach(products ->{
				dataProduct.add( new Products(products.getProductName(),products.getCategory(),
					products.getStock(),products.getPrice(),currentTime));
			});
			List<Products> addNew= repo.saveAll(dataProduct);
			
			if (addNew.size()>0) {
				return ResponseEntity.status(HttpStatus.OK).
					body(new ProductResponses(HttpStatus.OK.value(),"Add product successfully",addNew));
			} else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(new ProductResponses(HttpStatus.INTERNAL_SERVER_ERROR.value(),"Add product Fail",dataEmpty));
			}
		
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
				body(new ProductResponses(HttpStatus.INTERNAL_SERVER_ERROR.value(),"Add product Fail",dataEmpty));
		}
	}

	@PutMapping(value="/product/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Responses>  updateProduct(@PathVariable int id,@RequestBody Products payload) {
		dataProduct= repo.findProductId(id);
		if(dataProduct.size()==1) {
		int totalStock = dataProduct.get(0).getStock()+ payload.getStock();
		int updated = repo.update(id,payload.getProductName(),payload.getCategory(),
				totalStock,payload.getPrice());
		if(updated==1) {
			return ResponseEntity.status(HttpStatus.OK).
				body(new ProductResponses(HttpStatus.OK.value(),"Update product successfully",dataProduct));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
				body(new ProductResponses(HttpStatus.INTERNAL_SERVER_ERROR.value(),"Update product Fail",dataEmpty));
		}
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
				body(new ProductResponses(HttpStatus.INTERNAL_SERVER_ERROR.value(),"Update product Fail",dataEmpty));
		}
	}
	
	@DeleteMapping(value="/product/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Responses>  deleteProduct(@PathVariable int id) {
		dataProduct= repo.findProductId(id);
		
		if(dataProduct.size()==1) {
		int deleted = repo.delete(id);
		if(deleted==1) {
			return ResponseEntity.status(HttpStatus.OK).
				body(new ProductResponses(HttpStatus.OK.value(),"Delete product successfully",dataProduct));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(new ProductResponses(HttpStatus.INTERNAL_SERVER_ERROR.value(),"Delete product Fail",dataEmpty));
		}
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
					body(new ProductResponses(HttpStatus.INTERNAL_SERVER_ERROR.value(),"Delete product Fail",dataEmpty));
		}
	}
	
	//search 3 parameter product_name,category,stock > api/product/search?product_name='produk1'&category='category1'&stock=1
	@PostMapping(value="/product/search", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Responses>  search(@RequestParam String product_name,
			@RequestParam String category, @RequestParam int stock  ) {
		dataProduct =repo.findProduct(product_name,category, stock);
		
		if(dataProduct.size()>0) {
		return ResponseEntity.status(HttpStatus.OK).
				body(new ProductResponses(HttpStatus.OK.value(),"Product Found",dataProduct));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
				body(new ProductResponses(HttpStatus.INTERNAL_SERVER_ERROR.value(),"Product Not Found",dataEmpty));
		}
	}
}
