package com.nextsoft.mart.handler;

import java.util.List;

import com.nextsoft.mart.entity.Products;
import com.nextsoft.mart.entity.TR;

public class TRResponses implements Responses{
	private int status;
	private String message;
	private List<TR> data;
	private List<Products> product;
	public TRResponses() {}

	public TRResponses(int status, String message, List<TR> data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<TR> getData() {
		return data;
	}

	public void setData(List<TR> data) {
		this.data = data;
	}
	
	
}
