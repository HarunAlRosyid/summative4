package com.nextsoft.mart.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.nextsoft.mart.entity.TR;

public interface TRRepository extends JpaRepository<TR, Integer> {
	@Query(value="SELECT * FROM tr WHERE date(created_at)=?1", nativeQuery = true)
	List<TR> dailyTransaction(String createdAt);

	@Transactional
	@Modifying
	@Query (value="INSERT INTO tr (product_id,total_price,created_at,qty) VALUES (?1,?2,?3,?4)", nativeQuery = true)
	int add(int productId,int totalPrice, String currentTime,int qty );
	
	@Query(value="SELECT * FROM tr WHERE product_id=1", nativeQuery = true)
	List<TR> findProductId(int productId);
	
    @Query(value="SELECT * FROM tr ORDER BY id DESC LIMIT 1", nativeQuery = true)
    List<TR> lastOfRow();
}
