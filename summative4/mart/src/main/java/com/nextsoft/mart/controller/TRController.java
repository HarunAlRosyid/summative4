package com.nextsoft.mart.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nextsoft.mart.entity.Products;
import com.nextsoft.mart.entity.TR;
import com.nextsoft.mart.handler.Responses;
import com.nextsoft.mart.handler.TRResponses;
import com.nextsoft.mart.service.ProductRepository;
import com.nextsoft.mart.service.TRRepository;

@RestController
@RequestMapping("/api")
public class TRController {
	
	List<TR> transaction = new ArrayList<TR>();
	List<TR> dataEmpty = new ArrayList<TR>();
	String currentTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	@Autowired
	TRRepository repo;
	@Autowired
	ProductRepository repoProduct;
	
	
	@GetMapping(value="/payments", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Responses> getDailyTransaction() {
		transaction = repo.findAll();
		if(transaction.size()==0) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
				body(new TRResponses(HttpStatus.INTERNAL_SERVER_ERROR.value(),"List of Produts is empty",dataEmpty));
		} else {
			return ResponseEntity.status(HttpStatus.OK).
				body(new TRResponses(HttpStatus.OK.value(),"List of Produts",transaction));
		}	
	}
	
	@GetMapping(value="/payment/search", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Responses> getAllProduts(@RequestBody TR payload) {
		transaction = repo.dailyTransaction(payload.getCreatedAt());

		if(transaction.size()==0) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
				body(new TRResponses(HttpStatus.INTERNAL_SERVER_ERROR.value(),"List of Produts is empty",dataEmpty));
		} else {
			return ResponseEntity.status(HttpStatus.OK).
				body(new TRResponses(HttpStatus.OK.value(),"List of Produts",transaction));
		}	
	}
	
	@PostMapping(value="/payment", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Responses> addTransaction(@RequestBody TR payload) {
		List<Products> products = repoProduct.findProductId(payload.getProductId());
		int compare =products.get(0).getStock()-payload.getQty();
		if(compare<0) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
				body(new TRResponses(HttpStatus.INTERNAL_SERVER_ERROR.value(),"Insufficient stock ",dataEmpty));
		} else {
			int total= products.get(0).getPrice()*payload.getQty();
			repo.add(payload.getProductId(), total, currentTime, payload.getQty());
			transaction = repo.lastOfRow();
			repoProduct.transaction(payload.getProductId() ,compare);
			return ResponseEntity.status(HttpStatus.OK).
				body(new TRResponses(HttpStatus.OK.value(),"Trasaction Success",transaction));
		}	
	}

}
