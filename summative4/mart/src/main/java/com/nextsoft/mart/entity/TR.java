package com.nextsoft.mart.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "tr")
public class TR {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name="product_id")
	private int productId;
	@Column(name="total_price")
	private int totalPrice;
	@Column(name="created_at")
	private String createdAt;
	private int qty;
	
	
	public TR() {
	}
	
	public TR(int id, int productId, int totalPrice, String createdAt, int qty) {
		super();
		this.id = id;
		this.totalPrice = totalPrice;
		this.createdAt = createdAt;
		this.qty = qty;
		this.productId = productId;
	}

	public TR(int productId, int totalPrice, String createdAt, int qty) {
		super();
		this.totalPrice = totalPrice;
		this.createdAt = createdAt;
		this.qty = qty;
		this.productId = productId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

}
