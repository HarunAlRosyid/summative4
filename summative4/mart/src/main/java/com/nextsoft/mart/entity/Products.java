package com.nextsoft.mart.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "products")
public class Products {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name="product_name")
	private String productName;
	private String category;
	private int stock;
	private int price;
	@Column(name="created_at")
	private String createdAt;
	
	
	public Products() {
		super();
	}


	public Products( String productName, String category, int stock, int price, String createdAt) {
		super();
		this.productName = productName;
		this.category = category;
		this.stock = stock;
		this.price = price;
		this.createdAt = createdAt;
	}

	public Products(int id, String productName, String category, int stock, int price, String createdAt) {
		super();
		this.id = id;
		this.productName = productName;
		this.category = category;
		this.stock = stock;
		this.price = price;
		this.createdAt = createdAt;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
		
}
