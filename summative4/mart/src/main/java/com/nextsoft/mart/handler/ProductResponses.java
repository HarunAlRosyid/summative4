package com.nextsoft.mart.handler;

import java.util.List;

import com.nextsoft.mart.entity.Products;

public class ProductResponses implements Responses{
	private int status;
	private String message;
	private List<Products> data;
	
	public ProductResponses() {}

	public ProductResponses(int status, String message, List<Products> data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Products> getData() {
		return data;
	}

	public void setData(List<Products> data) {
		this.data = data;
	}
	
	
}
